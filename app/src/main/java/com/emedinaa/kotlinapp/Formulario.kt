package com.emedinaa.kotlinapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_formulario.*

class Formulario : AppCompatActivity() {
    private var edNombre : String = ""
    private var edEmail : String = ""
    private var edPassword : String = ""


    private fun ui() {
        btnCrearCuenta.setOnClickListener {
        if (ValidarFormulario()){
            MostrarMensaje()
        }


        }

    }
    private fun ValidarFormulario() : Boolean {
    Limpiar()
         edNombre = edtName.text.toString()
         edEmail  = edtEmail.text.toString()
         edPassword = edtPassword.text.toString()

        if (edNombre.isEmpty()){
            edtName.error = "Ingrese el nombre"
            return false
        }
        if (edEmail.isEmpty()){
            edtEmail.error = "Ingrese el Email"
            return false
        }
        if (edPassword.isEmpty()){
            edtPassword.error = "Ingrese la constraseña"
            return false
        }

        if (!edEmail.isValidEmail()){
            edtEmail.error = "Ingrese un Email valido"
            return false
        }


        return true
    }
    private fun Limpiar() {
        edtEmail.error = null
        edtName.error = null
        edtPassword.error = null
    }

    private fun MostrarMensaje() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)   //apply
        builder.setTitle("Ingreso")
        builder.setMessage("Ingreso al formulario")
        builder.setPositiveButton("Aceptar",null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_formulario)
        ui()

    }
}