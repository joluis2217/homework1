package com.emedinaa.kotlinapp

import android.util.Patterns

fun String.isValidEmail():Boolean =
    Patterns.EMAIL_ADDRESS.matcher(this).matches()
